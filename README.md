# Example create gist op

This is a simple example of a "@cto.ai" op that creates a github gist. This op can be run in the cli (using the @cto.ai tool https://cto.ai/docs/getting-started) or over slack (with the @cto.ai slackbot https://cto.ai/docs/slackapp)

# Building from source and running

+ clone this repo
+ ensure @cto.ai/ops is installed
+ build the op
```
ops build .
```

+ this op requires a github personal token set into the ops vault
(alternatively it can be inputted manually during runtime)
```
ops secrets:set
```
+ run the op
```
ops run .
```

To run in slack:
+ publish the op to your team that is associated with the slackbot
```
ops publish .
```
