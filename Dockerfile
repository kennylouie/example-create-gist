FROM registry.cto.ai/official_images/bash:latest

WORKDIR /ops

RUN apt update

RUN apt install -y jq

ADD gist.sh .
