#!/bin/bash

set -u

URL='https://api.github.com/gists'

main()
{
	need_cmd bash-sdk
	need_cmd jq
	need_cmd curl
	need_cmd sed
	need_cmd tr

	local token="$(get_github_token)"
	check_empty "${token}"

	local gist
	get_gist

	local resp="$(create_gist "${token}" "${gist}")"
	check_resp "${resp}"

	say Done!
}

create_gist()
{
	curl -s -H "Authorization: token $1" \
		"${URL}" \
		-X POST \
		-d "$2"
}

get_github_token()
{
	bash-sdk prompt secret \
		-n "token" \
		-m "github personal token" | jq --raw-output '.token'
}

get_gist()
{
	local desc="$(get_desc)"
	check_newline "${desc}"

	local filename="$(get_filename)"
	check_newline "${filename}"

	local content="$(get_content | sed 's/^```//' | sed 's/```$//' | sed 's/\\/\\\\/g' | sed 's/\"/\\"/g' | sed 's/$/\\n/' | tr -d '\n')"
	check_empty_content "${content}"

	local public="$(get_public)"

	local files="\"${filename}\": {\"content\": \"${content}\"}"
	gist="{\"description\": \"${desc}\", \"public\": ${public}, \"files\": {${files}}}"
}

get_desc() {
	bash-sdk prompt input -a \
		--message "description" \
		--name "desc" | jq --raw-output '.desc'
}

get_public()
{
	bash-sdk prompt autocomplete \
		--message "gist is public?" \
		-d "false" \
		-c "true" "false" \
		--name "public" | jq --raw-output '.public'
}

get_confirmation()
{
	bash-sdk prompt confirm \
		-default-true \
		--message "add a gist file" \
		--name "confirm" | jq --raw-output '.confirm'
}

get_filename()
{
	bash-sdk prompt input -a \
		--message "filename" \
		--name "filename" | jq --raw-output '.filename'
}

get_content()
{
	bash-sdk prompt editor \
		--message "new file content" \
		--name "content" | jq --raw-output '.content'
}

# utility function to indicate to user that program is needed
need_cmd()
{
	if ! check_cmd "$1"; then
		err "need $1 (command not found)"
	fi
}

# utility function to check if a program exectuable is callable
check_cmd()
{
	type "$1" >/dev/null 2>&1
}

# utility function to echo error and exit program
err()
{
	say "$@" >&2
	exit 1
}

# utility function to print with a tagline
say()
{
	bash-sdk print "$(printf "%b" "@cto.ai/ops: $@\n")"
}

# utility function to ensure variable is not empty
check_empty()
{
	[ -n "$1" ] || err "cannot be empty"
}

# utility function to ensure content is not empty
check_empty_content()
{
	[ "$1" != "\\n"  ] || err "cannot be empty"
}

# utility function to check response from curl requestes made to gists api
check_resp()
{
	[ ! -n "$(echo "$@" | jq '.message' >/dev/null 2>&1)" ] || err $(echo ${resp} | jq '.message')
}

# utility function to check for newline
check_newline()
{
	[[ ! "$@" == *$'\n'* ]] || err "cannot be multi-line"
}

# entry point function to script
main "$@"
